echo "###"
echo "Connecting to docker hub"
echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
echo "###"

echo "###"
echo "Building pecho_frontend"
docker build -t perecho/pecho_frontend:latest  -f dependencies.Dockerfile .
echo "###"

echo "###"
echo "Pushing pecho_frontend"
docker push perecho/pecho_frontend:latest
echo "###"

echo "###"
echo "Calling webhook"
curl -H "Authorization: $WEBHOOK_TOKEN" $WEBHOOK_URL
echo "###"
