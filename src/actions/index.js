export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const NW_CHAT_SUCCESS = 'NW_CHAT_SUCCESS';
export const NW_NEW_POST_REQUEST = 'NW_NEW_POST_REQUEST';
export const NEW_CHAT_SUCCESS = 'NEW_CHAT_SUCCESS';
export const SELECT_CHAT_ROOM = 'SELECT_CHAT_ROOM';
export const HISTORY_CHATROOM_SUCCESS = 'HISTORY_CHATROOM_SUCCESS';
export const CHATROOM_LIST_SUCCESS = 'CHATROOM_LIST_SUCCESS';

// Route actions below are also defined in routesMap
// Find a way to do this better, so there will be no
// Code duplication

export const HOME = 'HOME';
export const JAM = 'JAM';
export const REGISTRATION = 'REGISTRATION';
export const LOGIN = 'LOGIN';
export const CHAT = 'CHAT';
export const NW_CHAT_REQUEST = 'NW_CHAT_REQUEST';
export const NEW_CHAT_REQUEST = 'NEW_CHAT_REQUEST';
export const HISTORY_CHATROOM_REQUEST = 'HISTORY_CHATROOM_REQUEST';
export const CHATROOM_LIST_REQUEST = 'CHATROOM_LIST_REQUEST';
export const REGISTRATION_REQUEST = 'REGISTRATION_REQUEST';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';

