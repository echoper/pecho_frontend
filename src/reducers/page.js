import { HOME, REGISTRATION, LOGIN, NW_CHAT_REQUEST, CHAT, JAM } from '../actions/index';

export default function (state = 'HOME', action = {}) {
  switch (action.type) {
    case HOME:
      return action.type;
    case JAM:
      return action.type;
    case REGISTRATION:
      return action.type;
    case LOGIN:
      return action.type;
    case NW_CHAT_REQUEST:
      return action.type;
    case CHAT:
      return action.type;
    default:
      return state;
  }
}

