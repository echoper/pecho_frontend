import { NW_CHAT_SUCCESS } from '../actions/index';

export default function (state = {}, action = {}) {
  switch (action.type) {
    case NW_CHAT_SUCCESS:
      return action.payload.data;
    default:
      return state;
  }
}

