import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from '../actions/index';

const DEFAULT_LOGIN_STATE = {
  session: false,
  username: 'User not authenticated',
  roles: ['guest'],
};

export default function (state = DEFAULT_LOGIN_STATE, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        session: !!sessionStorage.loginToken,
        username: sessionStorage.getItem('loginToken')
          ? sessionStorage.username : 'User not authenticated',
        roles: ['member'],
      };
    case LOGOUT_SUCCESS:
      return {
        session: !!sessionStorage.loginToken,
        username: 'No username',
        roles: ['guest'],
      };
    default:
      return state;
  }
}
