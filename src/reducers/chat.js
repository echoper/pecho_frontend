import {
  NEW_CHAT_SUCCESS, HISTORY_CHATROOM_SUCCESS, CHATROOM_LIST_SUCCESS,
  SELECT_CHAT_ROOM,
} from '../actions/index';

export default function (state = {
  messages: [], history: [], roomTitle: '', roomPassword: '', chatrooms: [],
}, action = {}) {
  switch (action.type) {
    case NEW_CHAT_SUCCESS:
      return Object.assign({}, state, {
        messages: [
          ...state.messages,
          {
            text: action.payload.data.text,
            username: action.payload.data.username,
          },
        ],
      });
    case HISTORY_CHATROOM_SUCCESS:
      return Object.assign({}, state, {
        messages: [],
        history: action.payload.data.history,
      });
    case SELECT_CHAT_ROOM:
      return Object.assign({}, state, {
        roomTitle: action.payload.roomTitle,
        roomPassword: action.payload.roomPassword,
      });
    case CHATROOM_LIST_SUCCESS:
      return Object.assign({}, state, {
        chatroomList: action.payload.data.chatrooms,
      });
    default:
      return state;
  }
}

