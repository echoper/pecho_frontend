import axios from 'axios';
import { redirect } from 'redux-first-router';
import {
  LOGIN_REQUEST, LOGIN_SUCCESS, LOGOUT_SUCCESS, NW_CHAT_REQUEST, NW_CHAT_SUCCESS,
  HISTORY_CHATROOM_SUCCESS, CHATROOM_LIST_SUCCESS,
} from './actions/index';

const registerUser = async (dispatch, getState, { action }) => {
  const { username, password } = action.payload;
  const body = { username, password };
  const request = axios.post('/api/users', body);

  try {
    await request;
    const newAction = { type: LOGIN_REQUEST, payload: body };
    dispatch(newAction);
  } catch (err) {
    console.log(err);
  }
};

const loginUser = async (dispatch, getState, { action }) => {
  const { username, password } = action.payload;
  const auth = { username, password };
  const request = axios({ method: 'post', url: '/api/tokens', auth });
  try {
    const response = await request;
    sessionStorage.setItem('loginToken', response.data.token);
    sessionStorage.setItem('username', username);
    const newAction = { type: LOGIN_SUCCESS };
    dispatch(newAction);
    dispatch(redirect({ type: 'HOME' }));
  } catch (err) {
    console.log(err.response.data);
  }
};

const logoutUser = async (dispatch) => {
  const token = sessionStorage.getItem('loginToken');
  const authorizationHeader = { Authorization: `Bearer ${token}` };

  const request = axios({
    method: 'DELETE',
    url: '/api/tokens',
    headers: authorizationHeader,
  });
  try {
    await request;
    sessionStorage.removeItem('loginToken');
    sessionStorage.removeItem('username');
    const action = { type: LOGOUT_SUCCESS };
    dispatch(action);
  } catch (err) {
    sessionStorage.removeItem('loginToken');
    sessionStorage.removeItem('username');
    console.log(err);
  }
  dispatch(redirect({ type: 'HOME' }));
};

const nWChat = async (dispatch) => {
  const token = sessionStorage.getItem('loginToken');
  const authorizationHeader = { Authorization: `Bearer ${token}` };

  const request = axios({
    method: 'GET',
    url: '/api/NW_chat',
    headers: authorizationHeader,
  });

  try {
    const response = await request;
    const action = { type: NW_CHAT_SUCCESS, payload: response };
    dispatch(action);
  } catch (err) {
    console.log(err);
  }
};

const nWNewPost = async (dispatch, getState, { action }) => {
  const token = sessionStorage.getItem('loginToken');
  const authorizationHeader = { Authorization: `Bearer ${token}` };

  const { text } = action.payload;
  const data = { text };
  const request = axios({
    method: 'POST',
    url: '/api/NW_chat',
    data,
    headers: authorizationHeader,
  });

  try {
  // eslint-disable-next-line
    const response = await request;
    const newAction = { type: NW_CHAT_REQUEST };
    dispatch(newAction);
  } catch (err) {
    console.log(err);
  }
};

const chatHistory = async (dispatch, getState, { action }) => {
  const token = sessionStorage.getItem('loginToken');
  const authorizationHeader = { Authorization: `Bearer ${token}` };

  const { roomTitle, roomPassword } = action.payload;
  const request = axios({
    method: 'GET',
    url: `/api/chat?title=${roomTitle}&password=${roomPassword}`,
    headers: authorizationHeader,
  });

  try {
    const response = await request;
    const newAction = { type: HISTORY_CHATROOM_SUCCESS, payload: response };
    dispatch(newAction);
  } catch (err) {
    console.log(err);
  }
};

const chatroomList = async (dispatch, getState) => {
  const token = sessionStorage.getItem('loginToken');
  const authorizationHeader = { Authorization: `Bearer ${token}` };

  const request = axios({
    method: 'GET',
    url: '/api/chat/rooms',
    headers: authorizationHeader,
  });

  try {
    const response = await request;
    const action = { type: CHATROOM_LIST_SUCCESS, payload: response };
    dispatch(action);
  } catch (err) {
    console.log(err);
  }
};

// pathless routes need to come after routes!
export default {
  HOME: { path: '/' },
  JAM: { path: '/jam' },
  REGISTRATION: { path: '/registration' },
  LOGIN: { path: '/login' },
  LOGOUT_REQUEST: { path: '/logout', thunk: logoutUser, role: 'member' },
  NW_CHAT_REQUEST: { path: '/NW_chat', thunk: nWChat, role: 'member' },
  CHAT: { path: '/chat', role: 'member' },
  CHATROOM_LIST_REQUEST: { thunk: chatroomList },
  HISTORY_CHATROOM_REQUEST: { thunk: chatHistory },
  NW_NEW_POST_REQUEST: { thunk: nWNewPost },
  REGISTRATION_REQUEST: { thunk: registerUser },
  LOGIN_REQUEST: { thunk: loginUser },
};
