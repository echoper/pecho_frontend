import { redirect } from 'redux-first-router';
import { LOGIN, LOGIN_SUCCESS } from './actions/index';

const isAllowed = (type, user, routesMap) => {
  const role = routesMap[type] && routesMap[type].role;

  if (!role) return true;
  if (!user.session) return false;
  return user.roles.includes(role);
};

export default {
  onBeforeChange: (dispatch, getState, { action }) => {
    dispatch({ type: LOGIN_SUCCESS });
    const { login, location: { routesMap } } = getState();
    const allowed = isAllowed(action.type, login, routesMap);

    if (!allowed) {
      const newAction = redirect({ type: LOGIN });
      dispatch(newAction);
    }
  },
};
