import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import App from './containers/App';
import configureStore from './configureStore';
import registerServiceWorker from './registerServiceWorker';

const history = createHistory();
const { store } = configureStore(history);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();

export default store;
