import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { connectRoutes } from 'redux-first-router';

import routesMap from './routesMap';
import options from './options';
import * as reducers from './reducers';

export default (history) => {
  const { reducer, middleware, enhancer } = connectRoutes(history, routesMap, options);

  const rootReducer = combineReducers({ location: reducer, ...reducers });
  const middlewares = applyMiddleware(middleware);
  const store = createStore(rootReducer, compose(enhancer, middlewares));

  return { store };
};
