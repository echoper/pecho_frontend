import io from 'socket.io-client';
import store from './index';
import { NEW_CHAT_SUCCESS } from './actions/index';

export const socket = io({ transports: ['websocket'] });

socket.on('response', (response) => {
  store.dispatch({ type: NEW_CHAT_SUCCESS, payload: response });
});

export const emitMessage = (channel, message) => {
  socket.emit(channel, message);
};
