import React from 'react';
import MessageList from './MessageList';
import ChatRoomList from './ChatRoomList';
import { ChatFlex } from './messageStyledComponents';

const Chat = () => (
  <ChatFlex>
    <ChatRoomList />
    <MessageList />
  </ChatFlex>
);

export default Chat;
