import styled from 'styled-components';
import theme from './theme';

export const ChatFlex = styled.div`
  display: flex;
  align-items: space-around;
`;

export const ChatListContainer = styled.div`
  padding: 1em;
  border-right: thin solid ${theme.colors.mGreen};
  width: 25%;
  color: ${theme.colors.mWhite};
  z-index:1;
`;

export const ChatListSticky = styled.div`
  position: sticky;
  top: 6em;
`;

export const RoomTitleContainer = styled.div`
  background-color: ${theme.colors.mGray};
  padding: 0.5em;
  cursor: pointer;
  &:hover {
    background-color: ${theme.colors.mLightGray}
  }
`;

export const ActiveRoomContainer = styled.div`
  background-color: ${theme.colors.mGreen}
  padding: 0.5em;
  cursor: pointer;
  &:hover {
    background-color: ${theme.colors.mLightGray}

`;

export const SelectRoomModal = styled.div`
  position: fixed;
  top: 0%;
  left: 0%;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  display: block;
`;

export const SelectRoomModalMain = styled.div`
  position: fixed;
  width: 50%;
  height: 50%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const SelectFormContainer = styled.div`
  background-color: ${theme.colors.mLightGray};
  padding: 4em;
  height: 100%;
`;

export const SelectFormHeading = styled.h1`
  display: box;
  color: ${theme.colors.mWhite};
`;

export const SelectFormButton = styled.button`
  margin-top: 20px;
  width: 100%;
  padding: 1em;
  border: none;
  border-radius: 2em;
  background-color: ${theme.colors.mGreen};
  color: ${theme.colors.mWhite};
`;

export const SelectFormLabel = styled.label`
  display: inline-block;
  margin-top: 20px;
  color: ${theme.colors.mWhite};
`;

export const SelectFormInput = styled.input`
  width: 100%;
  padding: 1em;
  border: thin solid ${theme.colors.mGreen};
  border-radius: 1em;
  background-color: ${theme.colors.mLightGray};
  color: ${theme.colors.mWhite};
`;

export const SelectFormText = styled.p`
  margin-top: 5px;
  color: ${theme.colors.mWhite};
  font-size: 0.75em;
`;

export const MessageContainer = styled.div`
  width: 100%;
  margin-left: 4em;
  margin-right: 4em;
`;

export const Message = styled.div`
  padding: 1em;
  border-top: 1px solid ${theme.colors.mGreen};
  background-color: ${theme.colors.mLightGray};
  color: ${theme.colors.mWhite};
`;

export const UserName = styled.span`
  color: ${theme.colors.mGreen};
`;

export const MessageTime = styled.span`
  padding-left: 1em;
  color: ${theme.colors.mGreen};
  font-size: 0.7em;
`;

export const MessageText = styled.p`
  margin-top: 0.5em;
  margin-bottom: 0;
`;

export const MessageForm = styled.form`
  display: flex;
  align-items: stretch;
  position: sticky;
  bottom: 0;
`;

export const MessageTextArea = styled.textarea`
  padding: 1em;
  width: 100%;
  height: 7em;
  resize: none;
  border: 1px solid ${theme.colors.mGreen};
  background-color: ${theme.colors.mGray};
  color: ${theme.colors.mWhite};
`;

export const MessageButton = styled.button`
  padding: 1em;
  height: 7em;
  border: 0;
  background-color: ${theme.colors.mGreen};
  color: ${theme.colors.mWhite};
`;
