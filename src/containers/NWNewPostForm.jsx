import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NW_NEW_POST_REQUEST } from '../actions/index';
import { MessageForm, MessageTextArea, MessageButton } from './messageStyledComponents';

class NWSNewPostForm extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };

    this.handleNewPost = this.handleNewPost.bind(this);
  }

  enterNewPost(e) {
    const enter = 13;
    if (e.keyCode === enter && !e.shiftKey) {
      e.preventDefault();
      this.props.dispatch({
        type: NW_NEW_POST_REQUEST,
        payload: { text: this.state.text },
      });
      this.setState({ text: '' });
    }
  }

  handleNewPost(e) {
    e.preventDefault();
    this.props.dispatch({
      type: NW_NEW_POST_REQUEST,
      payload: { text: this.state.text },
    });
    this.setState({ text: '' });
  }

  render() {
    return (
      <MessageForm>
          <MessageTextArea
            value={this.state.text}
            onChange={event => this.setState({ text: event.target.value })}
            onKeyDown={event => this.enterNewPost(event)}
            type="text"
            placeholder="New message..."
          />

        <MessageButton
          onClick={this.handleNewPost}
          type="submit"
        >
          New post
        </MessageButton>
      </MessageForm>
    );
  }
}

NWSNewPostForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(NWSNewPostForm);
