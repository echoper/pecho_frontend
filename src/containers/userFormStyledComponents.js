import Link from 'redux-first-router-link';
import styled from 'styled-components';
import theme from './theme';

export const UserFormButton = styled.button`
  margin-top: 20px;
  width: 100%;
  padding: 1em;
  border: 0;
  border-radius: 2em;
  background-color: ${theme.colors.mGreen};
  color: ${theme.colors.mWhite};
`;

export const UserFormLink = styled(Link)`
  padding-left: 5px;
  color: ${theme.colors.mGreen};
`;

export const UserFormContainer = styled.div`
  max-width: 512px;
  margin: 10% 15%;
  padding: 60px;
  background-color: ${theme.colors.mLightGray};
`;

export const UserFormHeading = styled.h1`
  display: box;
  color: ${theme.colors.mWhite};
`;

export const UserFormLabel = styled.label`
  display: inline-block;
  margin-top: 20px;
  color: ${theme.colors.mWhite};
`;

export const UserFormInput = styled.input`
  width: 100%;
  padding: 1em;
  border: thin solid ${theme.colors.mGreen};
  border-radius: 1em;
  background-color: ${theme.colors.mLightGray};
  color: ${theme.colors.mWhite};
`;

export const UserFormText = styled.p`
  margin-top: 5px;
  color: ${theme.colors.mWhite};
  font-size: 0.75em;
`;

