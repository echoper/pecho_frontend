import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { emitMessage } from '../webSocket';
import { SELECT_CHAT_ROOM, HISTORY_CHATROOM_REQUEST } from '../actions/index';
import { SelectFormContainer, SelectFormHeading, SelectFormButton, SelectFormLabel, SelectFormInput } from './messageStyledComponents';

class SelectChatRoomForm extends Component {
  constructor(props) {
    super(props);
    this.state = { roomTitle: this.props.roomTitle, roomPassword: '' };

    this.handleSelectChatRoom = this.handleSelectChatRoom.bind(this);
  }

  handleSelectChatRoom(e) {
    e.preventDefault();
    this.props.dispatch({
      type: SELECT_CHAT_ROOM,
      payload: { roomTitle: this.state.roomTitle, roomPassword: this.state.roomPassword },
    });

    emitMessage('join', {
      roomTitle: this.state.roomTitle,
      roomPassword: this.state.roomPassword,
      username: this.props.loginInfo.username,
    });
    this.props.dispatch({
      type: HISTORY_CHATROOM_REQUEST,
      payload: { roomTitle: this.state.roomTitle, roomPassword: this.state.roomPassword },
    });
    sessionStorage.setItem(this.state.roomTitle, this.state.roomPassword);
    this.props.handleModalHide(e, this.state.roomTitle);
  }

  render() {
    return (
      <SelectFormContainer>
        {
          this.props.createRoom
          ? <SelectFormHeading>Create a new chatroom!</SelectFormHeading>
          : <SelectFormHeading>Join a chatroom!</SelectFormHeading>
        }
        <form>
          <SelectFormLabel>Enter title of the chatroom:</SelectFormLabel>
          <SelectFormInput
            value={this.state.roomTitle}
            onChange={event => this.setState({ roomTitle: event.target.value })}
            type="text"
            placeholder="Enter the title for the chatroom..."
          />
          <SelectFormLabel>Enter password for the chatroom:</SelectFormLabel>
          <SelectFormInput
            value={this.state.roomPassword}
            onChange={event => this.setState({ roomPassword: event.target.value })}
            type="text"
            placeholder="Enter the password for the chatroom..."
          />

          <SelectFormButton
            onClick={this.handleSelectChatRoom}
            type="submit"
          >
            Select Chatroom
          </SelectFormButton>
        </form>
      </SelectFormContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginInfo: state.login,
    chat: state.chat,
  };
}

SelectChatRoomForm.propTypes = {
  loginInfo: PropTypes.shape({
    messages: PropTypes.array, username: PropTypes.string,
  }).isRequired,
};

export default connect(mapStateToProps)(SelectChatRoomForm);
