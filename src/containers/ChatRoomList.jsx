import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { CHATROOM_LIST_REQUEST, SELECT_CHAT_ROOM, HISTORY_CHATROOM_REQUEST } from '../actions/index';
import { emitMessage } from '../webSocket';
import RoomModal from './RoomModal';
import { ChatListSticky, ChatListContainer, RoomTitleContainer, ActiveRoomContainer, SelectFormButton, MessageButton } from './messageStyledComponents';

class ChatRoomList extends Component {
  constructor(props) {
    super(props);

    this.handleModalHide = this.handleModalHide.bind(this);
    this.handleCreateRoomModal = this.handleCreateRoomModal.bind(this);

    this.state = {
      activeRoom: '',
      showModal: false,
      createRoom: false,
    };
  }

  componentDidMount() {
    this.props.dispatch({ type: CHATROOM_LIST_REQUEST });
  }

  handleModalShow(e, title) {
    e.preventDefault();

    const roomPassword = sessionStorage.getItem(title);

    if (roomPassword) {
      this.props.dispatch({
        type: SELECT_CHAT_ROOM,
        payload: { roomTitle: title, roomPassword },
      });

      emitMessage('join', {
        roomTitle: title,
        roomPassword,
        username: this.props.loginInfo.username,
      });

      this.props.dispatch({
        type: HISTORY_CHATROOM_REQUEST,
        payload: { roomTitle: title, roomPassword },
      });
    } else {
      this.setState({
        showModal: true,
      });
    }

    this.setState({
      activeRoom: title,
    });
  }

  handleModalHide(e, roomTitle) {
    e.preventDefault();
    this.setState({
      showModal: false,
      createRoom: false,
      activeRoom: roomTitle,
    });
    this.props.dispatch({ type: CHATROOM_LIST_REQUEST });
  }

  handleCreateRoomModal(e) {
    e.preventDefault();
    this.setState({
      createRoom: true,
      showModal: true,
    });
  }

  render() {
    if (!this.props.chat.chatroomList) {
      return <div />;
    }

    const chatroomList = this.props.chat.chatroomList.map(room => (
      (this.state.activeRoom === room.title)
        ? <ActiveRoomContainer onClick={event => this.handleModalShow(event, room.title)}>
          {room.title}
        </ActiveRoomContainer>

        : <RoomTitleContainer onClick={event => this.handleModalShow(event, room.title)}>
          {room.title}
        </RoomTitleContainer>
    ));

    return (
      <ChatListContainer>
        <ChatListSticky>
          Select a room:
          {chatroomList}
          {
            this.state.showModal
              ? <RoomModal
                createRoom={this.state.createRoom}
                handleModalHide={this.handleModalHide}
                roomTitle={this.state.activeRoom}
              />
              : null
          }
          <SelectFormButton onClick={this.handleCreateRoomModal}>Create a new room</SelectFormButton>
        </ChatListSticky>
      </ChatListContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    chat: state.chat,
    loginInfo: state.login,
  };
}

export default connect(mapStateToProps)(ChatRoomList);
