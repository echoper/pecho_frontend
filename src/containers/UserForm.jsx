import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { LOGIN, LOGIN_REQUEST, REGISTRATION, REGISTRATION_REQUEST } from '../actions/index';
import { UserFormContainer, UserFormHeading, UserFormLabel, UserFormInput, UserFormButton, UserFormText, UserFormLink } from './userFormStyledComponents';

class UserForm extends Component {
  constructor(props) {
    super(props);
    this.state = { username: '', password: '' };

    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(e) {
    e.preventDefault();
    if (this.props.page === LOGIN) {
      this.props.dispatch({
        type: LOGIN_REQUEST,
        payload: { username: this.state.username, password: this.state.password },
      });
    }
    if (this.props.page === REGISTRATION) {
      this.props.dispatch({
        type: REGISTRATION_REQUEST,
        payload: { username: this.state.username, password: this.state.password },
      });
    }
    this.setState({ username: '', password: '' });
  }

  userAddress() {
    if (this.props.page === LOGIN) {
      return 'Welcome back!';
    }
    return 'Create an account!';
  }

  loginRegister() {
    if (this.props.page === LOGIN) {
      return 'Need an account?';
    }
    return 'Already have an account?';
  }
  loginRegisterLink() {
    if (this.props.page === LOGIN) {
      return (
        <UserFormLink to={{ type: REGISTRATION }}>
          Register
        </UserFormLink>
      );
    }
    return (
      <UserFormLink to={{ type: LOGIN }}>
        Login
      </UserFormLink>

    );
  }

  loginRegisterButton() {
    if (this.props.page === LOGIN) {
      return 'Login';
    }
    return 'Create account';
  }

  render() {
    return (
      <UserFormContainer>
        <UserFormHeading>
          {this.userAddress()}
        </UserFormHeading>
        <form>
          <UserFormLabel>
            Username:
          </UserFormLabel>
          <UserFormInput
            value={this.state.username}
            onChange={event => this.setState({ username: event.target.value })}
            type="text"
            placeholder="Enter username..."
          />

          <UserFormLabel>
            Password:
          </UserFormLabel>
          <UserFormInput
            value={this.state.password}
            onChange={event => this.setState({ password: event.target.value })}
            type="password"
            placeholder="Enter password..."
          />
          <UserFormButton
            onClick={this.handleLogin}
            type="submit"
          >
            {this.loginRegisterButton()}
          </UserFormButton>
        </form>
        <UserFormText>
          {this.loginRegister()}
          {this.loginRegisterLink()}
        </UserFormText>
      </UserFormContainer>
    );
  }
}

UserForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  page: state.page,
});

export default connect(mapStateToProps)(UserForm);
