import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'redux-first-router-link';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { HOME, REGISTRATION, NW_CHAT_REQUEST, CHAT, LOGIN, LOGOUT_REQUEST, JAM } from '../actions/index';
import UserDetail from './UserDetail';
import theme from './theme';

const StyledNav = styled.nav`
  position: sticky;
  top: 0;
`;

const StyledNavLink = styled(NavLink)`
  text-decoration: none;
  color: #edffff;
  display: block;
  padding: 30px;
  &.active {
    color: ${theme.colors.mGray};
  }
`;

const Container = styled.div`
  display: flex;
  justify-content: "flex-start";
  background-color: ${theme.colors.mGreen};
`;

const NavBar = ({ page, session }) => (
  <StyledNav>
    <Container>
      <StyledNavLink
        to={{ type: HOME }}
        exact={true}
      >
        Home
      </StyledNavLink>

      <StyledNavLink
        to={{ type: JAM }}
      >
        Jam
      </StyledNavLink>

      {!session.session &&
        <StyledNavLink
          to={{ type: REGISTRATION }}
        >
          Registration
        </StyledNavLink>
      }

      {session.session &&
        <StyledNavLink
          to={{ type: NW_CHAT_REQUEST }}
        >
         Message board
        </StyledNavLink>
      }

      {session.session &&
        <StyledNavLink
          to={{ type: CHAT }}
        >
          Chat
        </StyledNavLink>
      }


      {!session.session &&
        <StyledNavLink
          to={{ type: LOGIN }}
        >
          Login
        </StyledNavLink>
      }

      {session.session &&
        <StyledNavLink
          to={{ type: LOGOUT_REQUEST }}
        >
          Logout
        </StyledNavLink>
      }
      {session.session &&
        <UserDetail />
      }
    </Container>
  </StyledNav>
);

NavBar.propTypes = {
  session: PropTypes.shape({
    session: PropTypes.bool,
  }).isRequired,
};


function mapStateToProps(state) {
  return {
    page: state.page,
    session: state.login,
  };
}

export default connect(mapStateToProps)(NavBar);
