import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import HomePage from './HomePage';
import UserForm from './UserForm';
import NWChat from './NWChat';
import Chat from './Chat';
import Jam from './Jam';
import { HOME, REGISTRATION, LOGIN, NW_CHAT_REQUEST, CHAT, JAM } from '../actions/index';

const Switcher = ({ page }) => {
  if (page === HOME) {
    return <HomePage />;
  }

  if (page === JAM) {
    return <Jam />;
  }

  if (page === REGISTRATION || page === LOGIN) {
    return <UserForm />;
  }

  if (page === NW_CHAT_REQUEST) {
    return <NWChat />;
  }

  if (page === CHAT) {
    return <Chat />;
  }

  return <div />;
};

Switcher.propTypes = {
  page: PropTypes.string.isRequired,
};


const mapStateToProps = state => ({
  page: state.page,
});

export default connect(mapStateToProps)(Switcher);
