import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SelectChatRoomForm from './SelectChatRoomForm';
import { SelectRoomModal, SelectRoomModalMain } from './messageStyledComponents';

const RoomModal = ({ roomTitle, handleModalHide, createRoom }) => (
  <SelectRoomModal onClick={handleModalHide}>
    <SelectRoomModalMain onClick={e => e.stopPropagation()}>
      <SelectChatRoomForm
        roomTitle={roomTitle}
        handleModalHide={handleModalHide}
        createRoom={createRoom}
      />
    </SelectRoomModalMain>
  </SelectRoomModal>
);

function mapStateToProps(state) {
  return {
    chat: state.chat,
    loginInfo: state.login,
  };
}

export default connect(mapStateToProps)(RoomModal);
