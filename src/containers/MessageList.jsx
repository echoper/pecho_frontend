import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import NewMessageForm from './NewMessageForm';
import { MessageContainer, Message, UserName, MessageTime, MessageText } from './messageStyledComponents';

class MessageList extends Component {
  constructor(props) {
    super(props);

    this.messageEnd = React.createRef();
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom() {
    if (this.messageEnd.current) {
      this.messageEnd.current.scrollIntoView({ behavior: 'smooth' });
    }
  }

  render() {
    if (!this.props.chat.messages) {
      return <div />;
    }

    const messageHistory = this.props.chat.history.map(message => (
      <Message>
        <UserName>{message.username}</UserName>
        <MessageTime>{message.created_at}</MessageTime>
        <MessageText>{message.text}</MessageText>
      </Message>
    ));

    const messageList = this.props.chat.messages.map(message => (
      <Message>
        <UserName>{message.username}</UserName>
        <MessageTime>{message.created_at}</MessageTime>
        <MessageText>{message.text}</MessageText>
      </Message>
    ));

    return (
      <MessageContainer>
        {messageHistory}
        {messageList}
        <div ref={this.messageEnd} />
        <NewMessageForm />
      </MessageContainer>
    );
  }
}

MessageList.propTypes = {
  chat: PropTypes.shape({ messages: PropTypes.array }).isRequired,
};


const mapStateToProps = state => ({
  chat: state.chat,
});

export default connect(mapStateToProps)(MessageList);
