import React from 'react';
import NWNewPostForm from './NWNewPostForm';
import NWPostList from './NWPostList';

const NWSChat = () => (
  <div>
    <NWPostList />
    <NWNewPostForm />
  </div>
);

export default NWSChat;
