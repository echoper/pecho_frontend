import { injectGlobal } from 'styled-components';
import theme from './theme';

export default {
  colors: {
    mWhite: '#edffff',
    mGreen: '#1dd171',
    mOlive: '#81a84d',
    mGray: '#36393f',
    mLightGray: '#42464d',
  },
};

injectGlobal`
  * {
    box-sizing: border-box;
  }
  body {
    background-color: ${theme.colors.mGray};
    margin: 0;
  }
`;
