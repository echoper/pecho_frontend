import React from 'react';
import styled from 'styled-components';
import theme from './theme';

const Container = styled.div`
  margin: 0 20%;
  padding: 1em;
  background-color: ${theme.colors.mLightGray};
  color: ${theme.colors.mWhite};
`;

const Warning = styled.h4`
  padding: 1em;
  color: yellow;
  border: thin solid red;
`;

const HomePage = () => (
  <Container>
    <h1>About</h1>

    <Warning>
      Please note: This is a work in progress and it's not ready for production.
      As such it's only meant for testing and playing around. Do not share any
      personal or private information.
    </Warning>

    <h2>What is the purpose of this project?</h2>
    <p>This is a showcase project to show my abilities to potential employers.</p>

    <h2>What can you do on the website?</h2>
    <p>In order to use it, you need to register an account or login if you already have one.</p>
    <p>Once you register you will gain access to:</p>
    <ul>
      <li>
        A Message Board where you can post new messages that are visible to everyone
        with an account.
      </li>
      <li>
        A chat app where you can create new chatrooms or join already created ones by other users.
        In order to join a room, you need to know its password.
      </li>
    </ul>

    <h2>Technical details</h2>
    <h3>Tools and technology I've used for this application</h3>
    <h4>Backend</h4>
    <ul>
      <li>Python3</li>
      <li>Flask</li>
      <li>Flask-HTTPAuth</li>
      <li>SQLAlchemy, Flask-SQLAlchemy</li>
      <li>Flask-SocketIO</li>
      <ul>
        <li>A websocket client for the backend</li>
        <li>I use it for /chat functionality</li>
      </ul>
      <li>redis, celery</li>
      <ul>
        <li>I use redis as a message broker for celery, which is a distributed task queue</li>
        <li>I offload saving new messages to the database (PostgreSQL) to celery
            when a new message is sent from frontend (/chat) via a websocket to the backend
        </li>
      </ul>
    </ul>

    <h4>Frontend</h4>
    <ul>
      <li>react</li>
      <li>redux</li>
      <li>redux-first-router</li>
      <ul>
        <li>Used for routing on the frontend</li>
      </ul>
      <li>socket.io-client</li>
      <ul>
        <li>A frontend websocket client</li>
      </ul>
      <li>styled-components</li>
      <ul>
        Used for styling react components
      </ul>
    </ul>

    <h4>Deployment</h4>
    <ul>
      <li>gitlab CI/CD tools</li>
      <li>docker</li>
      <li>saltstack</li>
      <ul>
        <li>A Python-based open source configuration management software</li>
      </ul>
      <li>flask</li>
      <li>nginx</li>
      <li>uWSGI</li>
      <li>let's encrypt</li>
    </ul>
    <h4>Overview of the whole deployment pipeline</h4>
    <ol>
      <li>Push to gitlab</li>
      <li>Gitlab's shared runner builds docker images for the project</li>
      <li>The runner uploads the images to docker hub</li>
      <li>The runner sends a request to the flask application on the master server</li>
      <li>The flask application sends a (job) request to saltstack's netapi</li>
      <ul>
        <li>
          This flask application is very basic. It's only job is to receive requests from
          github's runner, authenticate it via a token and pass the request forward as a job
          to saltstack's netapi
        </li>
      </ul>
      <li>The saltstack's reactor picks up the job and applies salt states to the
          slave server where this application is hosted
      </li>
      <li>Voila!</li>
    </ol>
    <h4>Design</h4>
      Design is most definitely a work in progress.

    <h4>ESSENTIAL TODO</h4>
    <ul>
      <li>testing</li>
      <li>form validation on frontend backend</li>
      <ul>
        <li>
          At the moment it's possible to send and empty post/message or create a chatroom
          without a title
        </li>
      </ul>
      <li>database migrations</li>
      <li>use env variables for configuration</li>
      <li>A friend crashed the backend yesterday. Look into it.</li>
      <li>improve the design</li>
    </ul>
  </Container>
);

export default HomePage;
