import React from 'react';
import NavBar from '../containers/NavBar';
import Switcher from '../containers/Switcher';

const App = () => (
  <div>
    <NavBar />
    <Switcher />
  </div>
);

export default App;
