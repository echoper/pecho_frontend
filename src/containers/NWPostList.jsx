import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from './theme';

const MessageContainer = styled.div`
`;

const Message = styled.div`
  padding: 1em;
  border-top: 1px solid ${theme.colors.mGreen};
  background-color: ${theme.colors.mLightGray};
  color: ${theme.colors.mWhite};
`;

const UserName = styled.span`
  color: ${theme.colors.mGreen};
`;

const MessageTime = styled.span`
  padding-left: 1em;
  color: ${theme.colors.mGreen};
  font-size: 0.7em;
`;

const MessageText = styled.p`
  margin-top: 0.5em;
  margin-bottom: 0;
`;


class NWPostList extends Component {
  constructor(props) {
    super(props);

    this.messagesEnd = React.createRef();
  }
  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom() {
    if (this.messagesEnd.current) {
      this.messagesEnd.current.scrollIntoView({ behavior: 'smooth' });
    }
  }

  render() {
    if (!this.props.nWChat.posts) {
      return <div />;
    }
    const postList = this.props.nWChat.posts.map(post => (
      <Message>
        <UserName>{post.username}</UserName>
        <MessageTime>{post.created_at}</MessageTime>
        <MessageText>{post.text}</MessageText>
      </Message>
    ));

    return (
      <MessageContainer>
        {postList}
        <div ref={this.messagesEnd} />
      </MessageContainer>
    );
  }
}

NWPostList.propTypes = {
  nWChat: PropTypes.shape({ posts: PropTypes.array }).isRequired,
};


const mapStateToProps = state => ({
  nWChat: state.nWChat,
});

export default connect(mapStateToProps)(NWPostList);
