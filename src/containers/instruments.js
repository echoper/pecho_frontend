import Tone from 'tone';

export default (instrument, chooseState) => {
  switch (instrument) {
    case 'piano': {
      const piano = new Tone.Sampler({
        A0: 'A0.mp3',
        C1: 'C1.mp3',
        'D#1': 'Ds1.mp3',
        'F#1': 'Fs1.mp3',
        A1: 'A1.mp3',
        C2: 'C2.mp3',
        'D#2': 'Ds2.mp3',
        'F#2': 'Fs2.mp3',
        A2: 'A2.mp3',
        C3: 'C3.mp3',
        'D#3': 'Ds3.mp3',
        'F#3': 'Fs3.mp3',
        A3: 'A3.mp3',
        C4: 'C4.mp3',
        'D#4': 'Ds4.mp3',
        'F#4': 'Fs4.mp3',
        A4: 'A4.mp3',
        C5: 'C5.mp3',
        'D#5': 'Ds5.mp3',
        'F#5': 'Fs5.mp3',
        A5: 'A5.mp3',
        C6: 'C6.mp3',
        'D#6': 'Ds6.mp3',
        'F#6': 'Fs6.mp3',
      }, {
        onload: () => {
          piano.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/piano/',
      }).toMaster();

      return piano;
    }

    case 'trumpet': {
      const trumpet = new Tone.Sampler({
        F2: 'F2.mp3',
        A2: 'A2.mp3',
        C3: 'C3.mp3',
        'D#3': 'Ds3.mp3',
        F3: 'F3.mp3',
        G3: 'G3.mp3',
        'A#3': 'As3.mp3',
        D4: 'D4.mp3',
        F4: 'F4.mp3',
        A4: 'A4.mp3',
        C5: 'C5.mp3',
      }, {
        onload: () => {
          trumpet.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/trumpet/',
      }).toMaster();

      return trumpet;
    }

    case 'acousticGuitar': {
      const acousticGuitar = new Tone.Sampler({
        D1: 'D1.mp3',
        E1: 'E1.mp3',
        'F#1': 'Fs1.mp3',
        G1: 'G1.mp3',
        'A#1': 'As1.mp3',
        C2: 'C2.mp3',
        'D#2': 'Ds2.mp3',
        E2: 'E2.mp3',
        'F#2': 'Fs2.mp3',
        G2: 'G2.mp3',
        'A#2': 'As2.mp3',
        C3: 'C3.mp3',
        'D#3': 'Ds3.mp3',
        E3: 'E3.mp3',
        'F#3': 'Fs3.mp3',
        G3: 'G3.mp3',
        'G#3': 'Fs3.mp3',
        'A#3': 'As3.mp3',
        B3: 'B3.mp3',
        'C#4': 'Cs4.mp3',
      }, {
        onload: () => {
          acousticGuitar.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/guitar-acoustic/',
      }).toMaster();

      return acousticGuitar;
    }

    case 'electricBass': {
      const electricBass = new Tone.Sampler({
        E2: 'E2.mp3',
        G2: 'G2.mp3',
        'A#2': 'As2.mp3',
        'C#3': 'Cs3.mp3',
        E3: 'E3.mp3',
        G3: 'G3.mp3',
        'A#3': 'As3.mp3',
        'C#4': 'Cs4.mp3',
        E4: 'E4.mp3',
        G4: 'G4.mp3',
        'A#4': 'As4.mp3',
        'C#5': 'Cs5.mp3',
        E5: 'E5.mp3',
        G5: 'G5.mp3',
        'A#5': 'As5.mp3',
        'C#6': 'Cs6.mp3',
      }, {
        onload: () => {
          electricBass.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/bass-electric/',
      }).toMaster();

      return electricBass;
    }

    case 'cello': {
      const cello = new Tone.Sampler({
        C2: 'C2.mp3',
        'D#2': 'Ds2.mp3',
        F2: 'F2.mp3',
        'G#2': 'Gs2.mp3',
        A2: 'A2.mp3',
        'C#3': 'Cs3.mp3',
        E3: 'E3.mp3',
        'F#3': 'Fs3.mp3',
        A3: 'A3.mp3',
        C3: 'C3.mp3',
        'D#3': 'Ds3.mp3',
        F3: 'F3.mp3',
        'G#3': 'Gs3.mp3',
        B3: 'B3.mp3',
        'C#4': 'Cs4.mp3',
        E4: 'E4.mp3',
        'F#4': 'Fs4.mp3',
        G4: 'G4.mp3',
        'A#4': 'As4.mp3',
        B4: 'B4.mp3',
        C5: 'C5.mp3',
      }, {
        onload: () => {
          cello.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/cello/',
      }).toMaster();

      return cello;
    }

    case 'bassoon': {
      const bassoon = new Tone.Sampler({
        G1: 'G1.mp3',
        A1: 'A1.mp3',
        C2: 'C2.mp3',
        G2: 'G2.mp3',
        A2: 'A2.mp3',
        C3: 'C3.mp3',
        E3: 'E3.mp3',
        G3: 'G3.mp3',
        A3: 'A3.mp3',
        C4: 'C4.mp3',
      }, {
        onload: () => {
          bassoon.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/bassoon/',
      }).toMaster();

      return bassoon;
    }

    case 'saxophone': {
      const saxophone = new Tone.Sampler({
        'C#2': 'Cs2.mp3',
        'D#2': 'Ds2.mp3',
        F2: 'F2.mp3',
        'G#2': 'Gs2.mp3',
        B2: 'B2.mp3',
        C3: 'C3.mp3',
        'D#3': 'Ds3.mp3',
        E3: 'E3.mp3',
        'F#3': 'Fs3.mp3',
        'G#3': 'Gs3.mp3',
        A3: 'A3.mp3',
        'C#4': 'Cs4.mp3',
        'D#4': 'Ds4.mp3',
        F4: 'F4.mp3',
        G4: 'G4.mp3',
        A4: 'A4.mp3',
      }, {
        onload: () => {
          saxophone.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/saxophone/',
      }).toMaster();

      return saxophone;
    }

    case 'electricGuitar': {
      const electricGuitar = new Tone.Sampler({
        'C#2': 'Cs2.mp3',
        E2: 'E2.mp3',
        'F#2': 'Fs2.mp3',
        A2: 'A2.mp3',
        C3: 'C3.mp3',
        'D#3': 'Ds3.mp3',
        'F#3': 'Fs3.mp3',
        A3: 'A3.mp3',
        C4: 'C4.mp3',
        'D#4': 'Ds4.mp3',
        'F#4': 'Fs4.mp3',
        A4: 'A4.mp3',
        C5: 'C6.mp3',
        'D#5': 'Ds5.mp3',
        'F#5': 'Fs5.mp3',
        A5: 'A5.mp3',
        C6: 'C6.mp3',
      }, {
        onload: () => {
          electricGuitar.triggerAttackRelease('C3', '6n');
          chooseState(instrument);
        },
        release: 1,
        baseUrl: './samples/guitar-electric/',
      }).toMaster();

      return electricGuitar;
    }

    default: {
      chooseState(instrument);
      const defaultInstrument = new Tone.PolySynth(17, Tone.Synth).toMaster();
      defaultInstrument.triggerAttackRelease(['C3'], '6n');
      return defaultInstrument;
    }
  }
};
