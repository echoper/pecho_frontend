import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { emitMessage } from '../webSocket';
import { MessageForm, MessageTextArea, MessageButton } from './messageStyledComponents';

class NewMessageForm extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };

    this.handleNewMessage = this.handleNewMessage.bind(this);
  }

  enterNewMessage(e) {
    const enter = 13;
    if (e.keyCode === enter && !e.shiftKey) {
      e.preventDefault();
      emitMessage('client message', {
        text: this.state.text,
        username: this.props.loginInfo.username,
        roomTitle: this.props.chat.roomTitle,
        roomPassword: this.props.chat.roomPassword,
      });
      this.setState({ text: '' });
    }
  }

  handleNewMessage(e) {
    e.preventDefault();
    emitMessage('client message', {
      text: this.state.text,
      username: this.props.loginInfo.username,
      roomTitle: this.props.chat.roomTitle,
      roomPassword: this.props.chat.roomPassword,
    });
    this.setState({ text: '' });
  }

  render() {
    return (
      <MessageForm>
        <MessageTextArea
          value={this.state.text}
          onChange={event => this.setState({ text: event.target.value })}
          onKeyDown={event => this.enterNewMessage(event)}
          type="text"
          placeholder="New message..."
        />

        <MessageButton
          onClick={this.handleNewMessage}
          type="submit"
        >
          New message
        </MessageButton>
      </MessageForm>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginInfo: state.login,
    chat: state.chat,
  };
}

NewMessageForm.propTypes = {
  loginInfo: PropTypes.shape({
    messages: PropTypes.array, username: PropTypes.string,
  }).isRequired,
};

export default connect(mapStateToProps)(NewMessageForm);
