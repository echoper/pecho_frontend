import React, { Component } from 'react';
import styled from 'styled-components';
import { emitMessage, socket } from '../webSocket';
import theme from './theme';
import loadInstrument from './instruments';


const Container = styled.div`
  margin: 0 20%;
  padding: 1em;
  background-color: ${theme.colors.mLightGray};
  color: ${theme.colors.mWhite};
  &:focus {
    background-color: ${theme.colors.mOlive};
  }
`;

const Indicator = styled.div`
  margin: 0 20%;
  padding: 1em;
  background-color: ${theme.colors.mOlive};
  color: ${theme.colors.mWhite};
  }
`;

const CurrentInstrumentIndicator = styled.span`
  margin: 0 5%;
  color: ${theme.colors.mGray};
`;

const chords = {
  1: ['C3', 'E3', 'G3'],
  2: ['D3', 'F3', 'A3'],
  3: ['E3', 'G3', 'B3'],
  4: ['F3', 'A3', 'C4'],
  5: ['G3', 'B3', 'D4'],
  6: ['A3', 'C3', 'E4'],
};

const soundMap = {
  a: { sound: ['C3'] },
  s: { sound: ['D3'] },
  d: { sound: ['E3'] },
  f: { sound: ['F3'] },
  g: { sound: ['G3'] },
  h: { sound: ['A3'] },
  j: { sound: ['B3'] },
  k: { sound: ['C4'] },
  l: { sound: ['D4'] },
  w: { sound: ['C#3'] },
  e: { sound: ['D#3'] },
  t: { sound: ['F#3'] },
  y: { sound: ['G#3'] },
  z: { sound: ['G#3'] },
  u: { sound: ['A#3'] },
  o: { sound: ['C#4'] },
  p: { sound: ['D#4'] },
  x: { sound: chords[1] },
  c: { sound: chords[2] },
  v: { sound: chords[3] },
  b: { sound: chords[4] },
  n: { sound: chords[5] },
  m: { sound: chords[6] },
};

const keySoundArray = 'a s d f g h j k l w e t y z y u o p x c v b n m'.split(' ');


class Jam extends Component {
  constructor(props) {
    super(props);

    this.keyPressed = {};

    this.updateInstrumentState = this.updateInstrumentState.bind(this);

    this.state = {
      instruments: {
        chosenInstrument: 'defaultInstrument',
        instrumentToLoad: 'defaultInstrument',
        defaultInstrument: { name: 'defaultInstrument', loaded: false },
        piano: { name: 'piano', loaded: false },
        trumpet: { name: 'trumpet', loaded: false },
        acousticGuitar: { name: 'acousticGuitar', loaded: false },
        electricBass: { name: 'electricBass', loaded: false },
        cello: { name: 'cello', loaded: false },
        bassoon: { name: 'bassoon', loaded: false },
        saxophone: { name: 'saxophone', loaded: false },
        electricGuitar: { name: 'electricGuitar', loaded: false },
      },
    };
  }

  componentDidMount() {
    const { instrumentToLoad } = this.state.instruments;
    this.defaultInstrument = loadInstrument(
      instrumentToLoad,
      this.updateInstrumentState,
    );

    this.defaultInstrument.triggerAttackRelease(['C4', 'E4', 'G4'], '8n');

    socket.on('start sound', (response) => {
      const { instruments } = this.state;
      const { sound, currentInstrument } = response.data;

      const instrumentLoaded = instruments[currentInstrument].loaded;
      const playInstrument = instrumentLoaded ? currentInstrument : 'defaultInstrument';

      switch (playInstrument) {
        case 'defaultInstrument':
          this.defaultInstrument.triggerAttack(sound);
          break;
        case 'piano':
          this.piano.triggerAttack(sound);
          break;
        case 'trumpet':
          this.trumpet.triggerAttack(sound);
          break;
        case 'acousticGuitar':
          this.acousticGuitar.triggerAttack(sound);
          break;
        case 'electricBass':
          this.electricBass.triggerAttack(sound);
          break;
        case 'cello':
          this.cello.triggerAttack(sound);
          break;
        case 'bassoon':
          this.bassoon.triggerAttack(sound);
          break;
        case 'saxophone':
          this.saxophone.triggerAttack(sound);
          break;
        case 'electricGuitar':
          this.electricGuitar.triggerAttack(sound);
          break;
        default:
          break;
      }
    });

    socket.on('stop sound', (response) => {
      const { instruments } = this.state;
      const { pkey, currentInstrument } = response.data;

      const pressedKeyMap = soundMap[pkey];
      const { sound } = pressedKeyMap;
      const instrumentLoaded = instruments[currentInstrument].loaded;

      const stopInstrument = instrumentLoaded ? currentInstrument : 'defaultInstrument';

      switch (stopInstrument) {
        case 'defaultInstrument':
          this.defaultInstrument.triggerRelease(sound);
          break;
        case 'piano':
          this.piano.triggerRelease(sound);
          break;
        case 'trumpet':
          this.trumpet.triggerRelease(sound);
          break;
        case 'acousticGuitar':
          this.acousticGuitar.triggerRelease(sound);
          break;
        case 'electricBass':
          this.electricBass.triggerRelease(sound);
          break;
        case 'cello':
          this.cello.triggerRelease(sound);
          break;
        case 'bassoon':
          this.bassoon.triggerRelease(sound);
          break;
        case 'saxophone':
          this.saxophone.triggerRelease(sound);
          break;
        case 'electricGuitar':
          this.electricGuitar.triggerRelease(sound);
          break;
        default:
          break;
      }
    });
  }

  updateInstrumentState(newInstrument) {
    const instruments = { ...this.state.instruments };

    instruments.chosenInstrument = newInstrument;
    instruments[newInstrument].loaded = true;

    this.setState({ instruments });
  }

  handleKeyDown(e) {
    e.preventDefault();
    const { chosenInstrument } = this.state.instruments;
    const skey = e.key;

    if (!this.keyPressed[skey] && keySoundArray.includes(skey)) {
      const soundData = soundMap[skey].sound;

      emitMessage('start sound', {
        sound: soundData,
        pkey: skey,
        currentInstrument: chosenInstrument,
      });

      this.keyPressed[skey] = true;
    }
  }

  handleKeyUp(e) {
    e.preventDefault();
    const { instruments } = this.state;
    const { chosenInstrument } = instruments;
    const skey = e.key;

    if (this.keyPressed[skey]) {
      emitMessage('stop sound', {
        pkey: skey,
        currentInstrument: chosenInstrument,
      });

      this.keyPressed[skey] = false;
    }

    const instrumentsSwitchArray = '1 2 3 4 5 6 7 8 9'.split(' ');

    if (instrumentsSwitchArray.includes(skey)) {
      const newInstruments = { ...instruments };

      if (skey === '1') {
        newInstruments.instrumentToLoad = 'defaultInstrument';
      } else if (skey === '2') {
        newInstruments.instrumentToLoad = 'piano';
      } else if (skey === '3') {
        newInstruments.instrumentToLoad = 'trumpet';
      } else if (skey === '4') {
        newInstruments.instrumentToLoad = 'acousticGuitar';
      } else if (skey === '5') {
        newInstruments.instrumentToLoad = 'electricBass';
      } else if (skey === '6') {
        newInstruments.instrumentToLoad = 'cello';
      } else if (skey === '7') {
        newInstruments.instrumentToLoad = 'bassoon';
      } else if (skey === '8') {
        newInstruments.instrumentToLoad = 'saxophone';
      } else if (skey === '9') {
        newInstruments.instrumentToLoad = 'electricGuitar';
      }

      this.setState({ instruments: newInstruments });
    }
  }

  render() {
    const { instruments } = this.state;
    const { chosenInstrument, instrumentToLoad } = instruments;

    if (chosenInstrument !== instrumentToLoad) {
      if (!instruments[instrumentToLoad].loaded) {
        switch (instrumentToLoad) {
          case 'defaultInstrument':
            this.defaultInstrument = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'piano':
            this.piano = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'trumpet':
            this.trumpet = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'acousticGuitar':
            this.acousticGuitar = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'electricBass':
            this.electricBass = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'cello':
            this.cello = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'bassoon':
            this.bassoon = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'saxophone':
            this.saxophone = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          case 'electricGuitar':
            this.electricGuitar = loadInstrument(
              instrumentToLoad,
              this.updateInstrumentState,
            );
            break;
          default:
            break;
        }
      } else {
        this.updateInstrumentState(instrumentToLoad);
      }

      return (
        <Container>
          <h1>Loading instrument. Please wait.</h1>
        </Container>
      );
    }

    return (
      <Container
        onKeyDown={event => this.handleKeyDown(event)}
        onKeyUp={event => this.handleKeyUp(event)}
        tabIndex="0"
      >
        <div>
          <Indicator>
            <h2>If the area below isn&#39;t the same color as this,
                the music won&#39;t play. Click here before you do
                anything.
            </h2>
          </Indicator>

          <p>Start the music with these keys:</p>
          <p>w, e, t, y/z, u, o, p</p>
          <p>a, s, d, f, g, h, j, k, l</p>
          <p>x, c, v, b, n, m</p>
          <p>And a one...</p>

          <h3>Current instrument:
            <CurrentInstrumentIndicator>{chosenInstrument}</CurrentInstrumentIndicator>
          </h3>
          <h4>You can choose between 9 instruments:</h4>
          <ul>
            <li>Press 1 for default instrument</li>
            <li>Press 2 for Piano (Download size: 5mb)</li>
            <li>Press 3 for Trumpet (Download size: 2mb)</li>
            <li>Press 4 for Acoustic Guitar (Download size: 4mb)</li>
            <li>Press 5 for Electric Bass (Download size: 5mb)</li>
            <li>Press 6 for Cello (Download size: 2mb)</li>
            <li>Press 7 for Bassoon (Download size: 2mb)</li>
            <li>Press 8 for Saxophone (Download size: 3mb)</li>
            <li>Press 9 for Electric Guitar (Download size: 2mb)</li>
          </ul>
        </div>
      </Container>
    );
  }
}

export default Jam;
