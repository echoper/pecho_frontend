import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from './theme';

const UserContainer = styled.div`
  margin-left: auto;
  color: ${theme.colors.mWhite};
  padding: 30px;
`;

const UserDetail = ({ detail }) => (
  <UserContainer>
    User:
    {detail.username}
  </UserContainer>
);

UserDetail.propTypes = {
  detail: PropTypes.shape({ username: PropTypes.string }).isRequired,
};


function mapStateToProps(state) {
  return {
    detail: state.login,
  };
}

export default connect(mapStateToProps)(UserDetail);
