FROM node:10.4-alpine

WORKDIR /pecho_frontend
COPY ./package.json ./package-lock.json /pecho_frontend/

RUN npm install

COPY . /pecho_frontend


RUN npm run build
